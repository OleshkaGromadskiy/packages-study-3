import 'package:flutter/material.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: AspectRatio(
            aspectRatio: 1.6,
            child: BlurHash(
              hash: 'LVKANY~qH=9FKOj?RjWVH=a0o~tR',
              image: 'https://i.ytimg.com/vi/CUHcheKJQAs/maxresdefault.jpg',
            ),
          ),
        ),
      ),
    );
  }
}
